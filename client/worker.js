console.log('Service worker loaded')

self.addEventListener('push', (e) => {
  const data = e.data.json()
  console.log('Push Recieved...')
  self.registration.showNotification(data.title, {
    body: 'Notified by JB!',
    icon:
      'https://banner2.cleanpng.com/20180328/efe/kisspng-logo-graphic-design-design-5abbdba715d519.2815318515222609030894.jpg'
  })
})
