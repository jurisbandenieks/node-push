const express = require('express')
const webpush = require('web-push')
const bodyParser = require('body-parser')
const path = require('path')

const app = express()

// Set static path
app.use(express.static(path.join(__dirname, 'client')))

app.use(bodyParser.json())

const publicVapidKey =
  'BDq00-bU8iqKIWDYG4ZWHuwMgdWy8plEaHPm7XTIqs7aawCIHz9zkBtnorg4Gp1TBJWOoI5Wmq8yrzSPnmh8bRU'
const privateVapidKey = '7mquajBnveCd6sDYkuHKFOxvGs6UT4L4CeYHJzeOCtQ'

webpush.setVapidDetails('mailto:test@test.com', publicVapidKey, privateVapidKey)

// Subscribe route
app.post('/subscribe', (req, res) => {
  // Get push subscribtion object
  const subscription = req.body

  // Send 201 - recourse created
  res.status(201).json({})

  // Create payload
  const payload = JSON.stringify({ title: 'Push test' })

  // Pass object into send notification
  webpush
    .sendNotification(subscription, payload)
    .catch((err) => console.error(err))
})

const port = 5000

app.listen(port, () => console.log(`Server started on port ${port}`))
